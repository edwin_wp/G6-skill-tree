import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import reportWebVitals from './reportWebVitals';
// import { Router } from "react-router";
import { BrowserRouter as BRouter , Redirect, Route, Switch, useHistory } from 'react-router-dom'
import Home from './comtainer/home/home.jsx'
import City from './comtainer/city/city.jsx'
import CityDetail from './comtainer/city/cityDetail/index.jsx'
import HomeDetail from './comtainer/home/homeDetail/detail.jsx'
import HomeDetail1 from './comtainer/home/homeDetail/detail1.jsx'
import FirstPage from './comtainer/index'
// eslint-disable-next-line react-hooks/rules-of-hooks
// const history = useHistory()
import {createBrowserHistory} from "history";
ReactDOM.render(
  <BRouter history={createBrowserHistory()}>
    <Switch>
      <Route exact path='/' component={FirstPage}></Route>
      <Route   path="/detail" render={() => 
        <Home>
          <Route exact path="/detail" component={HomeDetail}></Route>
          <Route exact  path="/detail/detail2" component={HomeDetail1}></Route>
        </Home>
      }></Route>
      {/* <Route  path="/" render={() => 
        <Home>
          <Redirect to="/detail"></Redirect>
          <Route path="detail" component={HomeDetail}/>
          <Route path="detail1" component={HomeDetail1}/>
        </Home>
      }></Route> */}
      <Route exact path="/city/:id" component={City}></Route>
      <Route  path="/citydetail" component={CityDetail}></Route>
      <Route  path="*" component={()=><div>哦豁，404喽！</div>}/>
    </Switch>
    
  </BRouter>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
