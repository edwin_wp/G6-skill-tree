import React from 'react';
import { Link, useHistory } from "react-router-dom";

// export default class City extends React.Component {
//     // eslint-disable-next-line no-useless-constructor
//     constructor(props) {
//         super(props)
//         // eslint-disable-next-line react-hooks/rules-of-hooks
//         // this.history = useHistory();
//     }
//     componentDidMount() {
//         console.log(this.props)
//     }
//     render() {
//         return (
//             <div>
//                 City
//                 {this.props.match.params.id}
//             </div>
//         )
//     }
// }
var userList = [{
    name: 'wp1',
    desc: 'xxxxxx1'
}, {
    name: 'wp12',
    desc: 'xxxxxx12'
}, {
    name: 'wp13',
    desc: 'xxxxxx13'
}, {
    name: 'wp14',
    desc: 'xxxxxx14'
}, {
    name: 'wp15',
    desc: 'xxxxxx15'
}]
export default function City(props) {
    const history = useHistory();
    function handleClick() {
        history.push("/detail");
    }
    console.log(props)
    return (
        <div >
            <h1 onClick={handleClick}> Go HomePage</h1>
            {props.match.params.id}
            {
               userList.map(item => {
                   return (
                       <div key={item.name} onClick={() => {
                            console.log('history', history)
                            history.push(`/citydetail`)
                       }}>
                           <p>{item.name}</p>
                           <p>{item.desc}</p>
                       </div>
                   )
               }) 
            }
        </div>
    )                      
}