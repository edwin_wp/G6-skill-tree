
import { Layout } from 'antd'
import G6 from '@antv/g6'
import React from 'react'
import { mockData } from '../data'
 
const { Content } = Layout
export default function HomeDetail(props) {
    React.useEffect(() => {
        const container = document.getElementById('container')
        const width = container.scrollWidth
        const height = container.scrollHeight || 500
        const graph = new G6.TreeGraph({
            container: 'container',
            width,
            height,
            modes: {//设置画布的交互模式
                default: ['collapse-expand', 'drag-canvas', 'zoom-canvas'],
            },
            defaultNode: {// 节点的配置，比如 type, size, color
                size: 26,
                anchorPoints: [// 节点连线的位置
                    [0, 0.5],
                    [1, 0.5],
                ],
            },
            defaultEdge: {//下边的配置，比如 type, size, color
                type: 'cubic-horizontal'
            },
            layout: {
                type: 'mindmap',
                direction: 'H',
                getWidth: (d) => {
                    return 20
                },
                getHeight: (d) => {
                    return 50
                },
                getgetHGa: (d) => {
                    return 50
                },
                getVGap: (d) => {
                    return 50
                }
            }
        })
        graph.data(mockData);
        graph.render();
        if (typeof window !== 'undefined') {
            window.onresize = () => {
                if (!graph || graph.get('destroyed')) return;
                if (!container || !container.scrollWidth || !container.scrollHeight) return;
                graph.changeSize(container.scrollWidth, container.scrollHeight);
            };
        } 
    })
    return (
        <div>
            <div onClick={() => props.history.push('/city/2')}>
                shouye
            </div>
            <div style={{height: '100vh', width: '100%'}} id="container">

            </div>
        </div>
        
    )
}