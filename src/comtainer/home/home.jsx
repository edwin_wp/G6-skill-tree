import React from 'react';

// import { useHistory } from "react-router-dom";

import CommonHeader from '../../components/commonHead'
import HomeDetail from './homeDetail/detail'

import { HashRouter as Router, Link, Route, useHistory } from 'react-router-dom'
// eslint-disable-next-line react-hooks/rules-of-hooks
// let history = useHistory()
export default class Home extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props)
        console.log('home', props)
        // eslint-disable-next-line react-hooks/rules-of-hooks
        // this.useHistory = useHistory()
    }
    // handleClick = () => {
    //     this.history.push('/city/3')
    // }
    render() {
        return (
            <div >
                <CommonHeader head="home"></CommonHeader>
                {/* <Link to="/detail">嵌套路由1</Link>
                <Link to="/detail1">嵌套路由2</Link> */}
                {/* <h1 onClick={() => 
                   this.props.history.push('/home/detail')
                }>Goto HomeDetail</h1> */}
                <Link to='/detail'>Goto HomeDetail1</Link>
                <Link to='/detail/detail2'>Goto HomeDetail2</Link>
                {this.props.children}
            </div>
        )
    }
}