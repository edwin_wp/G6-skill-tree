

import React from 'react'
export default class Counter extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props)
        this.state = {
            num: 1
        }
    }
    componentDidMount() {
        this.timeId = setInterval(() => {
            // 不能触发视图更新
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state.num = this.state.num + 1;
            // this.setState({num:this.state.num+1});
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    render() {
        return (
            <div >
                { this.props.count }
                <p> {this.state.num} </p>
            </div>
        );
    }
}



