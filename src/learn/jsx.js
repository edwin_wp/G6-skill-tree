

// jsX 1是一种js和html混合的语法，将组件结构、数据和样式混合在一起
// 2JSX其实只是一种语法糖,最终会通过babeljs转译成createElement语法
/**
 * <h1 className="title" style={{color:'red'}}>hello</h1>
    React.createElement("h1", {
        className: "title",
        style: {
        color: 'red'
        }
    }, "hello");
    {
        type:'h1',
        props:{
            className: "title",
            style: {
                color: 'red'
            }
        },
        children:"hello"
    }
 * 
 * */ 
    import RProps from './props1.js'
    import RState from './state2.js'
    import RStateAsync from './stateAsync3'
    // <Learn name='wp'></Learn>
    export default function Learn(props) {
        return (
            <div className="learn_jsx">
                <div>{`welcome,${props.name}`}</div>
                <RProps name="wenpeng"></RProps>
                <RState count='3' ></RState>
                <RStateAsync></RStateAsync>
            </div>
        )
    }
