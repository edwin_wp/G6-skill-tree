

/** state的更新可能是异步的
 * 出于性能考虑吧，react可能会异步更新
 * */ 

import React from 'react'

export default class Counter1 extends React.Component {
    // eslint-disable-next-line no-useless-constructor
    constructor(props) {
        super(props)
        this.state = {
            number: 0
        }
    }
    handleClick = () => {
        /**
         *  isBatchingUpdate 批量更新模式默认是true 
         *  如果参数是对象{...state,...nextState};
         *  如果参数是函数，先执行，再{...state,...nextState};
         * */ 
        console.log('11')
        this.setState({number:this.state.number+1});
        console.log(this.state.number);
        this.setState({number:this.state.number+1});

        this.setState((state)=>(
            {number:state.number+1}
        ));
        console.log(this.state.number);
        this.setState((state)=>(
            {number:state.number+1}
        ));
    }
    render() {
        return (
            <div>
                <p>wpeee{this.state.number}</p>
                <button onClick={this.handleClick}>点击</button>
            </div>
        )
    }
}