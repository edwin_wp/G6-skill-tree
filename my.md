## react的特点
- 采用组件化模式、声明式编码，提高开发效率及组件复用率
声明式编码： 改变某元素颜色，react交给react去操作dom
命令式: 获取元素节点，然后.style
- 在react native中可以使用react语法进行移动端开发
- 使用虚拟dom加diffing算法，尽量减少与真实dom的交互

## 创建
- create-react-app appname
